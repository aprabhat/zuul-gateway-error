#!/usr/bin/env bash

set -x
eval $(cat pid.txt)

kill -9 $EUREKA_PID
kill -9 $GATEWAY_PID
kill -9 $NOTIFICATION_PID

PROFILE=$1
echo "Selected Profile : "$PROFILE
RUNTIME_PROFILE=--spring.profiles.active=$PROFILE

#
EUREKA_JAR=eureka-0.0.1-SNAPSHOT.jar
GATEWAY_JAR=gateway-0.0.1-SNAPSHOT.jar
NOTIFICATION_JAR=test-0.0.1-SNAPSHOT.jar
#
MICROSERVICE_HOME=.
#
nohup java -jar $MICROSERVICE_HOME/eureka/target/$EUREKA_JAR > $MICROSERVICE_HOME/eureka/errorlog.out 2>&1 &
EUREKA_PID=$!
echo "EUREKA_PID"=$EUREKA_PID>pid.txt
#
nohup java -jar $MICROSERVICE_HOME/gateway/target/$GATEWAY_JAR > $MICROSERVICE_HOME/gateway/errorlog.out 2>&1 &
GATEWAY_PID=$!
echo "GATEWAY_PID"=$GATEWAY_PID>>pid.txt
#
nohup java -jar $MICROSERVICE_HOME/test/target/$NOTIFICATION_JAR > $MICROSERVICE_HOME/test/errorlog.out 2>&1 &
NOTIFICATION_PID=$!
echo "NOTIFICATION_PID"=$NOTIFICATION_PID>>pid.txt

set +x
