### To test the gateway service crash do the following
1. ```git clone https://aprabhat@bitbucket.org/aprabhat/zuul-gateway-error.git```
2. Go to the project directory and run ```mvn clean install```
3. Run project using ```./start.sh local```
4. To test, fire this url ```localhost:3000/api/notification/hello``` which should return 'Hello World'.

The logs are at {PROJECT_BASE_DIR}/gateway/errorlog.out. 
